#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import telegram
import os
import re
from tabulate import tabulate

from telegram.ext import Updater, CommandHandler
from pymongo import MongoClient
from titlecase import titlecase
TOKEN = os.environ.get('TELEGRAM_BOT_TOKEN')
MONGO_HOST = os.environ.get('TELEGRAM_MONGO_HOST', 'mongo')
MONGO_PORT = os.environ.get('TELEGRAM_MONGO_PORT', 27017)

vote_expression = re.compile(u'(?!\-\-|\+\+)(?P<votee>.+?)\s*(?P<operation>\-\-|\+\+|\u2014)\s*\w*\s*(?P<reason>.*)')

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

updater = Updater(token=TOKEN)

dispatcher = updater.dispatcher


def voteDB(collection):
    dbclient = MongoClient(MONGO_HOST, int(MONGO_PORT))
    db = dbclient.VOTE_DATABASE
    return db[collection]


def total_votes(cursor):
    target_total = 0
    for v in cursor:
        target_total += int(v['operation'])
    return target_total


def get_reasons(target, channel):
    reason_list = []
    vote_db = voteDB('votes')
    reasons = vote_db.find({'channel': channel, 'target': target}).distinct("reason")
    for r in reasons:
        user_reason_votes = vote_db.find({'channel': channel, 'target': target, 'reason': r})
        if not r:
            r = "No Reason"
        reason_list.append({'Reason': titlecase(r), 'Total': total_votes(user_reason_votes)})
    return reason_list


def wtf(bot, update):
    text = "What did I do wrong!?\nhttps://gitlab.com/one-t/telegram-bot/issues"
    bot.send_message(chat_id=update.message.chat_id, text=text)


def contribute(bot, update):
    text = "PRs accepted!\nhttps://gitlab.com/one-t/telegram-bot"
    bot.send_message(chat_id=update.message.chat_id, text=text)


def scoreboard(bot, update):
    vote_db = voteDB('votes')
    totals = []
    current_channel = update.message.chat_id
    channel_targets = vote_db.find({'channel': current_channel}).distinct("target")
    for u in channel_targets:
        target_votes = vote_db.find({"target": u,
                                     "channel": current_channel})
        target_total = total_votes(target_votes)
        totals.append({'name': u, 'total': target_total})
    sorted_totals = sorted(totals, key=lambda k: k['total'], reverse=True)

    scoreboard = tabulate(sorted_totals, headers="keys",  tablefmt="simple")
    bot.send_message(chat_id=update.message.chat_id,
                     text="<pre>{}</pre>".format(scoreboard),
                     parse_mode=telegram.ParseMode.HTML)


def vote(bot, update, args):
    v = vote_expression.match(' '.join(args))
    if not v:
        bot.send_message(chat_id=update.message.chat_id,
                         text="Vote example:\n`/vote glen-- for being a fuckboy`",
                         parse_mode=telegram.ParseMode.MARKDOWN)
        return
    op = v.group('operation')
    if op == '++':
        operation = 1
    if op == '--' or op == '\u2014':
        operation = -1
    if v.group('reason'):
        reason = v.group('reason')
    if not v.group('reason'):
        reason = None
    if v.group('reason'):
        reason = reason.lower()
    target = v.group('votee')
    vote = {'vote_ID': update.message.message_id,
            'channel': update.message.chat_id,
            'voter': update.message.from_user.id,
            'target': target.lower(),
            'operation': operation,
            'reason': reason}

    vote_db = voteDB('votes')
    vote_db.insert_one(vote)
    target_votes = vote_db.find({"target": vote['target'],
                                 "channel": vote['channel']})
    target_total = total_votes(target_votes)
    bot.send_message(chat_id=update.message.chat_id,
                     text="{} got {} for {}\nNew total is {}".format(titlecase(vote['target']),
                                                                     str(vote['operation']),
                                                                     str(vote['reason']),
                                                                     str(target_total)),
                     parse_mode=telegram.ParseMode.MARKDOWN)


def user_reasons(bot, update, args):
    vote_db = voteDB('votes')
    target = ' '.join(args).lower()
    if not vote_db.find_one({"channel": update.message.chat_id, "target": target}):
        bot.send_message(chat_id=update.message.chat_id,
                         text="What the fuck is a {}?".format(args),
                         parse_mode=telegram.ParseMode.MARKDOWN)
        return
    reason_scores = get_reasons(target, update.message.chat_id)
    sorted_totals = sorted(reason_scores, key=lambda k: k['Total'], reverse=True)
    reason_table = tabulate(sorted_totals, headers="keys",  tablefmt="simple")
    bot.send_message(chat_id=update.message.chat_id,
                     text="<pre>{}\n{}</pre>".format(titlecase(target), reason_table),
                     parse_mode=telegram.ParseMode.HTML)


vote_handler = CommandHandler('vote', vote, pass_args=True)
wtf_handler = CommandHandler('wtf', wtf)
contribute_handler = CommandHandler('contribute', contribute)
scoreboard_handler = CommandHandler('scoreboard', scoreboard)
reasons_handler = CommandHandler('reasons', user_reasons, pass_args=True)
dispatcher.add_handler(vote_handler)
dispatcher.add_handler(wtf_handler)
dispatcher.add_handler(contribute_handler)
dispatcher.add_handler(scoreboard_handler)
dispatcher.add_handler(reasons_handler)

updater.start_polling()
