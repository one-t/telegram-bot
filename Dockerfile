FROM python:3
ADD requirements.txt /
RUN pip install -r requirements.txt
ADD bot /bot
ENTRYPOINT python /bot/bot.py