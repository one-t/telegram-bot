# Telegram Karma Bot

This bot implements a karma system for telegram channels.

## Getting Started

### Requirements
* Docker

### Running the bot
1. Talk to the [botfather](https://telegram.me/BotFather) to get a token
2. Clone this repo
3. Create a Mongo container - `docker run --name bot-mongo -v /local/mongo/data/dir:/data/db --restart=unless-stopped -d mongo`
4. CD into the repo directory and build the container - `docker build -t telegram_bot:latest .`
5. Launch the container using your token - `docker run --link bot-mongo:mongo --name telegram_bot -e 'TELEGRAM_BOT_TOKEN=token_from_botfather' -d telegram_bot`